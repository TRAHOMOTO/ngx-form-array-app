import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Permissions} from '../permissions.enum';

@Component({
  selector: 'app-edit-form',
  templateUrl: './edit-form.component.html',
  styleUrls: ['./edit-form.component.css']
})
export class EditFormComponent implements OnInit {

  private _form: FormGroup;

  readonly permissions = Object.entries(Permissions)
    .filter(([title, value]) => typeof value === 'number')
    .map(([title, value]) => {
      return {title, value};
    });

  attributes: FormArray;

  get form() {
    return this._form;
  }

  @Input() itemForEdit: any /* ItemForEditInterface */;
  @Output() onFormSubmit = new EventEmitter<any>();

  constructor(private readonly fb: FormBuilder) { }

  ngOnInit() {
    console.log(Object.entries(Permissions));
    this.attributes = this.fb.array([]);

    this._form = this.fb.group({
      title: [null, Validators.required ],
      attributes: this.attributes
    });

    if (this.itemForEdit) {
      const ctrls = this.itemForEdit.attrs.map( itm => {
        return this.fb.group({
          name: [itm.permissionName, Validators.required],
          permissions: [itm.permissionFlag, Validators.required]
        });
      });

      this.attributes = this.fb.array(ctrls);

      this._form.setControl('attributes', this.attributes);

      this._form.patchValue(this.itemForEdit);
    }

  }

  isValid() {
    return this._form.valid;
  }

  onSubmit() {
    this.onFormSubmit.next(this.form.value);
  }
}
