export enum Permissions {
  See = 0,
  Read = 1,
  Write = 2,
  Delete = 3
}
