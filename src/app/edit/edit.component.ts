import {Component, OnInit, ViewChild} from '@angular/core';
import {EditFormComponent} from '../edit-form/edit-form.component';
import {Permissions} from '../permissions.enum';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  data = {
    title: 'Ololo foo bar',
    attrs: [
      {
        permissionName: 'Some name', permissionFlag: Permissions.See
      },
      {
        permissionName: 'Some name2', permissionFlag: Permissions.Read
      }
    ]
  };

  @ViewChild(EditFormComponent) readonly form: EditFormComponent;

  constructor() { }

  ngOnInit() {
  }

  onFormSubmit(formValues: any /*IFormValue*/) {
    if (!this.form.isValid()) {
      alert('FormInvalid');
    }
    console.log(formValues);
  }
}
